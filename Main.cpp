#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

int main()
{
	int n = 28;
	time_t t;
	time(&t);
	int day = (localtime(&t)->tm_mday) % n;

	const int size = 3;
	int list[size][size];
	int sum = 0;
	for (int i = 0;i < size;i++) {
		
		for (int j = 0;j < size; j++) {
			list[i][j] = i + j;
			std::cout << list[i][j] << " ";
			if (i == day) {
				sum += list[i][j];
			}
		}
		std::cout << "\n";
	};
	std::cout << "indexArray"<<" " << day <<"\n"<< "summa " << sum;

}